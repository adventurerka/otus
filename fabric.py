import json
import random
import unittest

SELECT = 'select'
MERGE = 'merge'
INSERTION = 'insertion'
SORT_TYPES = (SELECT, MERGE, INSERTION,)


class AbstractSorter:

    def sort_array(self, array):
        raise NotImplemented


class MergeSorter(AbstractSorter):
    """ сортировка слиянием
    """

    def merge_sort(self, array):

        if len(array) == 1:
            return
        middle = len(array) // 2
        left = array[:middle]
        right = array[middle:]
        self.merge_sort(left)
        self.merge_sort(right)

        i = j = k = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                array[k] = left[i]
                i += 1
            else:
                array[k] = right[j]
                j += 1
            k += 1

        while i < len(left):
            array[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            array[k] = right[j]
            j += 1
            k += 1

    def sort_array(self, array):
        self.merge_sort(array)


class InsertionSorter(AbstractSorter):
    """ сортировка вставками
    """

    @staticmethod
    def insertion_sort(array):
        for i in range(1, len(array)):
            key = array[i]
            j = i - 1
            while j >= 0 and key < array[j]:
                array[j + 1] = array[j]
                j -= 1
            array[j + 1] = key

    def sort_array(self, array):
        self.insertion_sort(array)


class SelectSorter(AbstractSorter):
    """ сортировка выбором
    """

    @staticmethod
    def select_sort(array):
        for i in range(len(array) - 1):
            m = i
            j = i + 1
            while j < len(array):
                if array[j] < array[m]:
                    m = j
                j += 1
            array[i], array[m] = array[m], array[i]

    def sort_array(self, array):
        self.select_sort(array)


class Application:

    def __init__(
            self,
            sort_from: str = 'sort_from',
            sort_to: str = 'sort_to',
            sort_type: str = None
    ):
        self.sort_from = sort_from
        self.sort_to = sort_to
        self.sort_type = sort_type

    def get_sorter(self):
        if self.sort_type == SELECT:
            return SelectSorter()
        elif self.sort_type == MERGE:
            return MergeSorter()
        else:
            return InsertionSorter()

    def sort_array(self):
        with open(self.sort_from, 'r') as storage:
            array = json.loads(storage.read())
            self.get_sorter().sort_array(array)
        with open(self.sort_to, 'w') as storage:
            storage.write(json.dumps([self.sort_type, array]))


class Test(unittest.TestCase):

    @staticmethod
    def _test_sort(sort_type_src):
        sort_from = 'sort_from_test'
        sort_to = 'sort_to_test'
        with open(sort_from, 'w') as storage:
            storage.write(json.dumps([
                random.randint(-10, 10) for _ in range(50)
            ]))
        Application(sort_from, sort_to, sort_type_src).sort_array()

        with open(sort_from, 'r') as storage:
            array_src = json.loads(storage.read())
            # встроенная питон функция сортировки
            array_src.sort()
        with open(sort_to, 'r') as storage:
            sort_type_dst, array_dst = json.loads(storage.read())

        assert sort_type_src == sort_type_dst
        assert array_src == array_dst

    def test_select_sort(self):
        self._test_sort(SELECT)

    def test_insertion_sort(self):
        self._test_sort(INSERTION)

    def test_merge_sort(self):
        self._test_sort(MERGE)


if __name__ == '__main__':
    unittest.main()
