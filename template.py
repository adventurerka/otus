import copy
import json
import unittest

TRANSPOSE = 'transpose'
SUM_MATRIX = 'sum_matrix'
DETERMINANT = 'get_determinant'


class AbstractCalculator:

    def calculate(self, src_file: str, dst_file: str):
        with open(src_file, 'r') as storage:
            data = storage.read()
        with open(dst_file, 'w') as storage:
            storage.write(self._calculate(data))

    def _calculate(self, src_data: str) -> str:
        raise NotImplemented


class TransposeActor(AbstractCalculator):
    """ Транспонирование матрицы
    """

    @staticmethod
    def _transpose(matrix: list) -> list:
        return [[row[index] for row in matrix] for index in
                range(len(matrix[0]))]

    def _calculate(self, src_data: str) -> str:
        return json.dumps(self._transpose(json.loads(src_data)))


class SumActor(AbstractCalculator):
    """ Суммирование двух матриц
    """

    @staticmethod
    def _sum_matrix(matrix_a: list, matrix_b: list) -> list:
        return [[
            matrix_b[row_index][column_index] + value
            for column_index, value in enumerate(row)
        ] for row_index, row in enumerate(matrix_a)]

    def _calculate(self, src_data: str) -> str:
        matrix_a, matrix_b = json.loads(src_data)
        return json.dumps(self._sum_matrix(matrix_a, matrix_b))


class GetDeterminantActor(AbstractCalculator):
    """ Вычисление определителя матрицы
    """

    def _get_determinant(self, matrix: list, total: int = 0) -> int:
        indices = list(range(len(matrix)))

        if len(matrix) == 2 and len(matrix[0]) == 2:
            val = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
            return val

        for column in indices:
            sub_matrix = copy.deepcopy(matrix)
            sub_matrix = sub_matrix[1:]

            for index in range(len(sub_matrix)):
                sub_matrix[index] = sub_matrix[index][0:column] + \
                                    sub_matrix[index][column + 1:]

            sign = (-1) ** (column % 2)
            sub_det = self._get_determinant(sub_matrix)
            total += sign * matrix[0][column] * sub_det

        return total

    def _calculate(self, src_data: str) -> str:
        return json.dumps(self._get_determinant(json.loads(src_data)))


class MatrixCalculator:

    @staticmethod
    def calculate(src_file: str, dst_file: str, operation_type: str):
        {
            TRANSPOSE: TransposeActor,
            SUM_MATRIX: SumActor,
            DETERMINANT: GetDeterminantActor
        }[operation_type]().calculate(src_file, dst_file)


class Test(unittest.TestCase):

    def _check_matrix_calculation(self, src_data, operation_type, dst_data):
        src_file = 'src_file'
        dst_file = 'dst_file'
        with open('src_file', 'w') as storage:
            storage.write(json.dumps(src_data))
        MatrixCalculator.calculate(src_file, dst_file, operation_type)
        with open(dst_file, 'r') as storage:
            self.assertEqual(dst_data, json.loads(storage.read()))

    def test_get_determinant(self):
        self._check_matrix_calculation(
            [[2, 1, 1, 1], [1, 2, 1, 1], [1, 1, 2, 1], [1, 1, 1, 2]],
            'get_determinant', 5
        )

    def test_transpose(self):
        self._check_matrix_calculation(
            [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
            'transpose', [[1, 4, 7], [2, 5, 8], [3, 6, 9]]
        )

    def test_sum_matrix(self):
        self._check_matrix_calculation(
            [[[1, 2, 3], [4, 5, 6]], [[1, 2, 3], [4, 5, 6]]],
            'sum_matrix', [[2, 4, 6], [8, 10, 12]]
        )


if __name__ == '__main__':
    unittest.main()
