from multiprocessing.dummy import Pool as ThreadPool
import unittest


def matrixmult(a, b):
    """ Произведение матриц
    """
    if len(a) == 0 or len(a[0]) != len(b):
        raise Exception('Не корректная размерность матриц для перемножения.')

    pool = ThreadPool(4)

    def func(args):
        return sum(ele_a * ele_b for ele_a, ele_b in zip(*args))

    return pool.map(
        lambda row_a: pool.map(
            func, [(row_a, col_b,) for col_b in list(zip(*b))]
        ), [row_a for row_a in a]
    )


class Test(unittest.TestCase):

    def test1(self):
       self.assertRaises(Exception, lambda: matrixmult([], []))

    def test2(self):
       self.assertEqual(matrixmult([[1]], [[1]]), [[1]])

    def test3(self):
       self.assertEqual(
           matrixmult([[1, 2], [1, 2]], [[1, 2], [1, 2]]),
           [[3, 6], [3, 6]]
       )

    def test4(self):
       self.assertEqual(
           matrixmult([[1], [2], [3]], [[1, 2, 3]]),
           [[1, 2, 3], [2, 4, 6], [3, 6, 9]]
       )

    def test5(self):
       self.assertRaises(Exception, lambda: matrixmult([[1, 1]], [[1]]))


if __name__ == '__main__':
    unittest.main()


