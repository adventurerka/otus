import json
import random
import unittest


class MatrixCreator:
    min_value = -10
    max_value = 10

    @property
    def storage_name(self) -> str:
        """ Название хранилища с созданными матрицам
        """
        return 'F2'

    def _generate(self, rows: int, columns: int) -> list:
        """ Создать матрицу заданной размерности rows X columns
        """
        return [[
            random.randint(self.min_value, self.max_value)
            for _ in range(columns)
        ] for __ in range(rows)]

    def generate_two_matrix(self, rows: int, columns: int):
        """ Создать и сохранить в хранилище storage_name
        2 матрицы заданной размерности rows X columns
        """
        with open(self.storage_name, 'w') as storage:
            storage.write(json.dumps([
                self._generate(rows, columns) for _ in range(2)
            ]))


class MatrixAdapter:

    def __init__(self, matrix_creator: MatrixCreator):
        self.storage_name = matrix_creator.storage_name

    def _read_matrix(self) -> str:
        """ Прочитать из хранилища storage_name матрицы
        """
        with open(self.storage_name, 'r') as storage:
            return storage.read()

    def adapt(self):
        """ Сохранить матрицы в хранилище F0
        """
        with open('F0', 'w') as storage:
            storage.write(self._read_matrix())


class MatrixCalculator:

    @staticmethod
    def _get_matrix() -> list:
        """ Прочитать из хранилища F0 матрицы
        """
        with open('F0', 'r') as storage:
            return json.loads(storage.read())

    @staticmethod
    def sum_matrix(matrix_a: list, matrix_b: list) -> list:
        """ Суммирование двух матриц
        """
        return [[
            matrix_b[row_index][column_index] + value
            for column_index, value in enumerate(row)
        ] for row_index, row in enumerate(matrix_a)]

    def sum_matrix_and_save(self):
        """ Сохранить результат суммирования матриц в хранилище F1
        """
        with open('F1', 'w') as storage:
            storage.write(json.dumps(self.sum_matrix(*self._get_matrix())))


class Test(unittest.TestCase):

    def test(self):
        creator = MatrixCreator()
        rows = 2
        columns = 3
        creator.generate_two_matrix(rows, columns)
        with open(creator.storage_name, 'r') as storage:
            matrix_a, matrix_b = json.loads(storage.read())

            for matrix in (matrix_a, matrix_b,):
                assert len(matrix) == rows
                for row in matrix:
                    assert len(row) == columns

            matrix_sum = MatrixCalculator.sum_matrix(matrix_a, matrix_b)

        adapter = MatrixAdapter(creator)
        adapter.adapt()

        calculator = MatrixCalculator()
        calculator.sum_matrix_and_save()

        with open('F1', 'r') as storage:
            assert matrix_sum == json.loads(storage.read())


if __name__ == '__main__':
    unittest.main()
